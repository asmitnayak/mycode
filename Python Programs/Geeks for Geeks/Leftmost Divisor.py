t = int(input())

for _ in range(t):
    p, q = map(int, input().split())
    pq = p ** q
    digits = list(map(int, str(pq)))
    flag = True
    if len(digits) > 18:
        digits = digits[:10]
        pq = int(''.join(map(str, digits)))
    for i in digits:
        if i != 0 and pq % i == 0:
            print(i)
            flag = False
            break
    if flag:
        print(-1)
