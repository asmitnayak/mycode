def isPrime(n):
    if n == 1:
        return False
    for i in range(2, n // 2 + 1):
        if n % i == 0:
            return False
    return True


t = int(input())

for _ in range(t):
    ab = input().split()
    a = int(ab[0])
    b = int(ab[1])
    for i in range(a, b + 1):
        if isPrime(i):
            print(i, end=" ")
    print()
