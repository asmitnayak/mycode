n = int(input())


for i in range(n):
    x = int(input())
    b = "" + bin(x)
    b = b[2:]
    revBin = list(("0" * (32 - len(b)) + b))
    revBin.reverse()
    print(int(''.join(revBin), 2))
