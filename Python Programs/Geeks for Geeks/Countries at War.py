t = int(input())

for _ in range(t):
    n = int(input())
    A = list(map(int, input().split()))
    B = list(map(int, input().split()))
    a, b = 0, 0
    for i in range(n):
        if A[i] > B[i]:
            b += 1
        elif A[i] < B[i]:
            a += 1
        else:
            a += 1
            b += 1
    print(n - a, n - b, end=" ")
    if a > b:
        print("B")
    elif a < b:
        print("A")
    else:
        print("DRAW")
