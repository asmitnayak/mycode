t = int(input())

for _ in range(t):
    h = int(input())
    d = 0
    while h > 0:
        d += 2 * h
        h = h // 2
    print(d)
