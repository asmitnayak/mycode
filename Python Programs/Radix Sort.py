# This program for radix-sort is NOT ideal
def getDigit(num, pos, maxlength):  # to get the digits at the pos-th position.
    # If length of digits is  less than maxlength the also add (maxlength - number od digits)
    # number of zeros
    numString = str(num)
    if len(numString) < maxlength:
        numString = "0" * (maxlength - len(numString)) + numString
    return int(numString[len(numString) - pos - 1])


def radixSort(d):   # returning a list of digits by pulling them out from bottom from 0 to 9
    newDigits = []
    for i in range(10):
        lst = d[i]
        for j in range(len(lst)):
            newDigits.append(lst[j])
    return newDigits

if __name__ == '__main__':
    n = int(input())
    digits = list(map(int, input().split()))
    maxx = max(digits)
    l = len(str(maxx))
    buckets = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
    x = 0
    while x < l:    # loop will run for l-times
        for j in range(len(digits)):
            buckets[getDigit(digits[j], x, l)].append(digits[j])    # pushing digits acc to their
                                                                #  positional value of j to the bucket
        digits = radixSort(buckets)  # sorting
        buckets = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}  # re-initialize
        x += 1
    print(' '.join(map(str, digits)))
