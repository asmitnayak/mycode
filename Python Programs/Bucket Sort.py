# This program is NOT ideal
import math
if __name__ == '__main__':
    n = int(input())
    num = list(map(int, input().split()))
    buckets = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
    divider = math.ceil(max(num) + 2) / (len(buckets))   # finding divider
    for i in num:
        j = i // divider    # the j hece obtained is used to assign arr[i](here, i) to buckets[j]
        buckets[j].append(i)
    for i in range(10):
        buckets[i].sort()    # sorting individual bucket
    newArr = []
    for i in range(10):
        for j in range(len(buckets[i])):
            newArr.append(buckets[i][j])    # appending sorted items of each bucket
    print(' '.join(map(str, newArr)))
