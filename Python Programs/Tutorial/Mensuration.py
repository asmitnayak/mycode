import math

a, p, d = 0, 0, 0.0
flag = False


def area():
    global l, b, a, flag
    a = l * b
    print("AREA IS:", a)
    flag = True


def per():
    global l, b, p, flag
    p = 2 * (l + b)
    print("PERIMETER IS:", p)
    flag = True


def dia():
    global l, b, d, flag
    d = math.sqrt(l**2 + b**2)
    print("DIAGONAL IS:", d)
    flag = True

switch = {
    'A': area,
    'P': per,
    'D': dia
}

itr = True

while itr:
    l, b = int(input("ENTER LENGTH: ")), int(input("ENTER BREADTH: "))
    ch = input("A. AREA\nP. PERIMETER\nD. DIAGONAL\nENTER CHOICE: ")
    ch = ch.upper()

    switch[ch]()

    if flag is False:
        print("WRONG CHOICE")
    itr = True if input("RE-TRY?(Y/N)").upper() == 'Y' else False

print("THANK YOU!")
