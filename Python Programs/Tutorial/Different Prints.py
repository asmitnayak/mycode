# using a comma (,) automatically adds a space before the next print element, eg:

print("Hello", "World")

# while using + we have to specify a space, eg:

print("Hello " + "World")

print(str(5) + "k")
# or
print(5, "k")
print(5, "k", sep="")
print("k", 5)
print("k", 5, sep="")
