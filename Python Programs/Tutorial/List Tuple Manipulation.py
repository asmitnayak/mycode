x1 = (5, 6, 2, 6)   # is a tuple, i.e., unmodifiable
x2 = 5, 6, 2, 6     # is a tuple, i.e., unmodifiable

y = [5, 6, 2, 4, 6, 8, 2, 3, 62, 8, 9, 2]      # A list, i.e., modifiable

# Append at the end
y.append(2)     # O/P: [5, 6, 2, 4, 6, 8, 2, 3, 62, 8, 9, 2, 2]

# Insert at a position
y.insert(2, 99)  # O/P: [5, 6, 99, 2, 4, 6, 8, 2, 3, 62, 8, 9, 2, 2]

# Remove the first occurrence of a number
y.remove(62)    # O/P: [5, 6, 99, 2, 4, 6, 8, 2, 3, 8, 9, 2, 2]
y.remove(y[8])  # O/P: [5, 6, 99, 2, 4, 6, 8, 2, 8, 9, 2, 2] removes first occurrence of y[8] = 3

# Print a group of indices
print(y[5:9])   # O/P: [6, 8, 2, 8]

# Print last of the list use negative indices
print(y[-2])    # O/P: 2

# Finding index value
print(y.index(8))   # O/P: 6

# Find the number of occurrence of a number
print(y.count(8))   # O/P: 2

# Sort list
y.sort()    # in argument put reverse = true to sort in descending
