n = int(input("ENTER A NUMBER: "))
k = 0

for i in range(2, int(n / 2)):
    if n % i == 0:
        k += 1
        break

if k > 0:
    print("NOT PRIME")
else:
    print("PRIME")
