import time

ticks = time.time()     # expresses the number of seconds since 12:00 AM January 1, 1970 to today
# in seconds
print(ticks)

# time.localtime(s) returns a time-tuple followings 's' seconds since epoch if nothing is
# specified then the current time is returned
t = time.localtime(ticks)
print(t)    # e.g.: time.struct_time(tm_year=2018, tm_mon=6, tm_mday=12, tm_hour=23,
# tm_min=37, tm_sec=0, tm_wday=1, tm_yday=163, tm_isdst=0)

# time.asctime({time-tuple}) returns the time in time-tuple in string format
ascT = time.asctime(t)
print(ascT)     # e.g.: Tue Jun 12 23:39:53 2018

# time.ctime(s) returns the time since 's' seconds from epoch in string format
# if none then current time (in Local Time)
cTym = time.ctime()
print(cTym)     # e.g.: Tue Jun 12 23:43:12 2018

# time.gmtime(s) returns the time-tuple since 's' seconds from epoch time
# if no args is passed then current time is returned (in UTC Time)
utcT = time.gmtime()
print(utcT)

# time.clock() returns current CPU time
clcT = time.clock()
print(clcT)     # e.g.: 5.401956912911271e-07

# time.mktime({time-tuple}) accepts time tuple and returns it into seconds
secs = time.mktime(t)
print(secs)

# time.sleep(s) is used to suspend execution for 's' seconds
print("Current Time: " + time.asctime())
t0 = time.clock()
t1 = time.time()
time.sleep(2.55)
print("SUSPENDED FOR 2.5 SECONDS")
print("Current Time: " + time.asctime())
print("Process Time: " + str(t0 - time.clock()))
print("Current Time: " + str(t1 - time.time()))
