a, b, c = int(input("a: ")), int(input("b: ")), int(input("c: "))

max = (a if a > c else c) if a > b else (b if b > c else c)

if a == b == c:
    print("ALL EQUAL")
    exit(0)

print(max)
