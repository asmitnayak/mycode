# Used as an alternate for switch case
# Dictionary is a un-ordered array of specific keys

dict = {
    0: "zero",
    1: "one",
    2: "two",
    3: "three"}

print(dict)

# Add a key
dict[4] = "four"

print(dict)

# Update a key - NO DUPLICATE KEYS ARE ALLOWED
dict[4] = "upDated"

print(dict)

# Remove a key
del dict[4]

print(dict)

exDict2 = {
    1: ("one", "First"),
    2: ("two", "Second"),
    3: ("three", "Third")
}

print(exDict2)
print(exDict2[2])       # ('two', 'Second')
print(exDict2[2][1])    # Second
