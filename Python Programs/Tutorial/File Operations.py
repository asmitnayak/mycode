# There are 3 type of operation:
# 1. Write -> 'w'       clears all data in the file
# 2. Append -> 'a'      adds to the already available data
# 3. Read -> 'r'
# The open functions opens the text file if it exits or else creates it
# WRITING
text = "Hello World\nLearning Python"

file1 = open('textFile.txt', 'w')
file1.write(text)
file1.close()
# APPENDING
text2 = "\nAppending Now!!!\n"

file2 = open('textFile.txt', 'a')
file2.write(text2)
file2.close()
# READING
readText = open('textFile.txt', 'r').read()
print(readText)

# reading line by line
readTextList = open('textFile.txt', 'r').readlines()  # creates List
print(readTextList)
