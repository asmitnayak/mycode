inp = input("ENTER A STRING: ")

l = len(inp)
f = 0

for i in range(0, int(l / 2)):
    if inp[i] != inp[l - i - 1]:
        f += 1
        break
    i += 1
if f == 1:
    print("NOT PALINDROME")
else:
    print("PALINDROME")
