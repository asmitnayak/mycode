x = 5
y = 5


def change():
    # print(x)    prints 5
    # x+= 4 -> error variable not initialised
    # workaround 1:
    global x    # telling the compiler that here x is a global variable
    print(x)    # prints 5
    x += 4      # updates x to 5+4=9
    print(x)
    # workaround 2:
    globy = y
    print(globy)    # prints 5
    globy += 4
    print(globy)    # prints 9
    return globy

y = change()   # y is updated to the returned value of globy = 9
print(y)
