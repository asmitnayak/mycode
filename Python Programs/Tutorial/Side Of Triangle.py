a, b, c = int(input("SIDE 1: ")), int(input("SIDE 2: ")), int(input("SIDE 3: "))

if a == b == c:
    print("EQUILATERAL TRIANGLE")
elif a == b or b == c or a == c:
    print("ISOSCELES TRIANGLE")
else:
    print("SCALENE TRIANGLE")
