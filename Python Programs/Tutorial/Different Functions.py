# TYPE 1 - this is call-by-value
def add(num1, num2):
	sum = num1 + num2
	print(sum)


add(5, 6)


# TYPE 2 - this is a different type of call which allows you to manipulate the formal argument list sequence
# if the name of formal parameters are used.

def add2(num1, num2):
	print('this is num1:', num1)
	sum2 = num1 + num2
	print(sum2)


add2(5, 6)
add2(num2=5, num1=6)


# add2(num3=3,num4=4) -> error
# TYPE 3 - this type allows you to give default values and can be implemented with type 2:

def add3(num1, num2=4, num3=8):
	print('num1 is:', num1)
	print('num3 is:', num3)
	sum3 = num1 + num2 + num3
	print(sum3)


add3(3);
add3(3, num3=6)
