import calendar

# calendar.calendar(year,w,l,c,m) returns a multi-line string formatted into 3 columns
# separated by 'c' spaces, 'w' width in characters of each date and l is the length of line
# each line has length =  21*w + 18 + 2*c
print(calendar.calendar(2018))

# calendar.month(year,month,w,l) returns a multi-line string having the calendar for the
# specified month of the specified year
print(calendar.month(2018, 8))

# calendar.firstweekday() return the current setting for starting weekday, 0 is Monday
print("1st Week Day:", calendar.firstweekday())

# calendar.isleap(year) returns boolean True or False is year is leap-year
print("2016 is leap year", calendar.isleap(2016))
print("2015 is leap year", calendar.isleap(2015))

# calendar.leapdays(y1,y2) returns number of leap days in the range of y1 and y2
print("Number of Leap Day(s) is:", calendar.leapdays(2012, 2016))

# calendar.monthcalendar(year,month) returns a list of list of ints having the days of the
# month specified staring from 1 where each sublist is a week days outside the month are 0
print(calendar.monthcalendar(2018, 8))

# calendar.monthrange(year,month) returns 2 integer tuple where first integer is the weekday code
# of the first day of the month, e.g., 2 is for Wednesday and 0 is for Monday
# and the second integer is the number of days in the month
print(calendar.monthrange(2018, 8))

# calendar.setfirstweekday(weekday) sets the first day of the week normally 0 is Monday
calendar.setfirstweekday(2)
print("Now 1st Week Day:", calendar.firstweekday())

calendar.setfirstweekday(0)     # reset to default

# calendar.weekday(year,month,day) returns the weekday code for the give day from
# 0 (Monday) to 6 (Sunday)
print("10th of August is", calendar.weekday(2018, 8, 10))   # 4 is Friday
