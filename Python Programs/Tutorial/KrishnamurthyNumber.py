n = int(input("ENTER A NUMBER: "))

a = n
sum = 0

while a > 0:
    fact = 1
    remain = int(a % 10)
    for i in range(1, remain + 1):
        fact *= i
    sum += fact
    a = int(a / 10)

if sum == n:
    print(n, "IS A KRISHNAMURTHY NUMBER")
else:
    print(n, "IS NOT A KRISHNAMURTHY NUMBER")
