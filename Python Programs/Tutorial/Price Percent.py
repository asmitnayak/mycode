cp = int(input("ENTER CP: "))
sp = int(input("ENTER SP: "))

p = sp - cp

p = (p / cp) * 100

if p > 0:
    print("PROFIT: ", p, "%", sep="")
else:
    print("LOSS: ", (p * -1), "%", sep="")
