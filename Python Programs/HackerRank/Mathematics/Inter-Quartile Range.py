def median(num):
    if len(num) % 2 == 0:
        return (num[len(num) // 2] + num[len(num) // 2 - 1]) / 2
    else:
        return num[len(num) // 2]

n = int(input())
x = list(map(int, input().split()))
frq = list(map(int, input().split()))
X = []
for i in range(n):
    for j in range(frq[i]):
        X.append(x[i])
X.sort()
l = []
u = []
if len(X) % 2 == 0:
    l = X[:len(X) // 2]
    u = X[len(X) // 2:]
else:
    l = X[:len(X) // 2]
    u = X[len(X) // 2 + 1:]
Q1 = float(median(l))
Q3 = float(median(u))
print(round(Q3 - Q1, 1))
