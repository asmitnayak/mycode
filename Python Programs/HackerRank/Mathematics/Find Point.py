def findPoint(px, py, qx, qy):
    dx = qx - px
    dy = qy - py
    rx = qx + dx
    ry = qy + dy
    print(rx, ry)    


n = int(input())

for n_itr in range(n):
    pxPyQxQy = input().split()

    px = int(pxPyQxQy[0])

    py = int(pxPyQxQy[1])

    qx = int(pxPyQxQy[2])

    qy = int(pxPyQxQy[3])

    result = findPoint(px, py, qx, qy)
