def median(num):
    if len(num) % 2 == 0:
        return (num[len(num) // 2] + num[len(num) // 2 - 1]) / 2
    else:
        return num[len(num) // 2]

n = int(input())
X = list(map(int, input().split()))
X.sort()
Q2 = int(median(X))
l = []
u = []
if len(X) % 2 == 0:
    l = X[:len(X) // 2]
    u = X[len(X) // 2:]
else:
    l = X[:len(X) // 2]
    u = X[len(X) // 2 + 1:]
Q1 = int(median(l))
Q3 = int(median(u))
print(Q1, Q2, Q3, sep='\n')
