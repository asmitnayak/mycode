from math import sqrt

n = int(input())
X = list(map(int, input().split()))

mu = sum(X) / n

var = 0.0
for i in X:
    var += (i - mu) ** 2
var = var / n
stdDev = sqrt(var)
print(round(stdDev, 1))
