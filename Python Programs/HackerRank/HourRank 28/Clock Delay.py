# Complete the lagDuration function below.
def lagDuration(h1, m1, h2, m2, k):
    # Return an integer denoting the duration of time in minutes by which the clock has been lagging.
    clockDur = m2 - m1 + (h2 - h1) * 60
    actDur = k * 60
    lag = actDur - clockDur
    return lag


q = int(input())

for q_itr in range(q):
    h1M1H2M2 = input().split()

    h1 = int(h1M1H2M2[0])

    m1 = int(h1M1H2M2[1])

    h2 = int(h1M1H2M2[2])

    m2 = int(h1M1H2M2[3])

    k = int(input())

    result = lagDuration(h1, m1, h2, m2, k)
    print(result)
