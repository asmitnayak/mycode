N = int(input())
lst = []

for i in range(N):

    cmd, *values = input("").split()

    if cmd == "insert":
        lst.insert(int(values[0]), int(values[1]))
    elif cmd == "remove":
        lst.remove(int(values[0]))
    elif cmd == "print":
        print(lst)
    elif cmd == "append":
        lst.append(int(values[0]))
    elif cmd == "sort":
        lst.sort()
    elif cmd == "pop":
        lst.pop()
    else:
        lst.reverse()
