def jumpingOnClouds(c, k):
    i = 0
    e = 100
    n = len(c)
    while True:
        jump = (i + k) % n
        if c[jump] == 1:
            e -= 3
        else:
            e -= 1
        i = jump
        if i == 0:
            break
    print(e)


nk = input().split()

n = int(nk[0])

k = int(nk[1])

c = list(map(int, input().rstrip().split()))

result = jumpingOnClouds(c, k)
