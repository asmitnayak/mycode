def print_formatted(n):
    w = len(str(bin(n))[2:])
    print(w)
    for i in range(1, n + 1):
        b = "" + bin(i)
        b = b[2:]
        o = "" + oct(i)
        o = o[2:]
        h = "" + hex(i)
        h = h[2:]
        d = "" + str(i)
        print(d.rjust(w), o.rjust(w), h.rjust(w).upper(), b.rjust(w))


n = int(input())
print_formatted(n)
