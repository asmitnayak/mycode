n = int(input())
x = list(map(int, input().split()))
w = list(map(int, input().split()))
prod = 0
for i in range(n):
    prod += x[i] * w[i]
mean = round(prod / sum(w), 1)
print(mean)
