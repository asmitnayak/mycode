n = int(input())
num = list(map(int, input().split()))
mean = sum(num) / len(num)
num.sort()
median = 0
if len(num) % 2 == 0:
    median = num[len(num) // 2] + num[len(num) // 2 - 1]
    median /= 2
else:
    median = num[len(num) // 2]
mode = max(num, key=num.count)
print(mean)
print(median)
print(mode)
