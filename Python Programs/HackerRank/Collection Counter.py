from collections import Counter

X = int(input(""))
sizes = Counter(list(map(int, input().split())))
N = int(input(""))
total = 0
for _ in range(N):
    inp = input().split()
    size = int(inp[0])
    price = int(inp[1])
    if sizes[size] > 0:
        total += price
        sizes[size] -= 1
print(total)
