kevin = 0
stuart = 0


def minion_game(st):
    vowels = ['A', 'E', 'I', 'O', 'U']
    global kevin, stuart
    sub = []
    for i in range(len(st)):
        for j in range(i, len(st)):
            sub.append(st[i:j + 1])
    for i in range(len(sub)):
        if sub[i][0] in vowels:
            kevin += 1
        else:
            stuart += 1
    print(kevin, stuart)

s = input()
minion_game(s)
