def climbingLeaderboard(scores, alice):
    scores.sort(reverse=True)
    # print(scores)
    i = len(scores) - 1
    while i > -1:
        if scores[i] > alice[0]:
            print(i + 2)
            alice.remove(alice[0])
        else:
            i -= 1
    if len(alice) > 0:
        print("1\n"*len(alice))


scores_count = int(input())
scores = list(set(list(map(int, input().rstrip().split()))))
alice_count = int(input())
alice = list(map(int, input().rstrip().split()))
result = climbingLeaderboard(scores, alice)
