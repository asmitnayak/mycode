def maximizingXor(l, r):
    maxx = 0
    for i in range(l, r + 1):
        for j in range(i, r + 1):
            maxx = max(i ^ j, maxx)
    print(maxx)


l = int(input())

r = int(input())

result = maximizingXor(l, r)
