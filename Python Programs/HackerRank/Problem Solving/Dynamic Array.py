def dynamicArray(n, queries):
    seqList = {}
    for i in range(n):
        seqList[i] = []
    lastAnswer = 0
    arr = []
    for query in queries:
        x = query[1]
        y = query[2]
        if query[0] == 1:
            seqList[(x ^ lastAnswer) % n].append(y)
        else:
            lastAnswer = seqList[(x ^ lastAnswer) % n][y % (len(seqList[(x ^ lastAnswer) % n]))]
            arr.append(lastAnswer)
    print('\n'.join(map(str, arr)))

nq = input().split()

n = int(nq[0])

q = int(nq[1])

queries = []

for _ in range(q):
    queries.append(list(map(int, input().rstrip().split())))

dynamicArray(n, queries)
