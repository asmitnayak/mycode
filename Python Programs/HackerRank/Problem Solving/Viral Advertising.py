def viralAdvertising(n):
    shared = 5
    liked = 0
    cum = 0
    for i in range(1, n + 1):
        liked = shared // 2
        cum += liked
        shared = (shared // 2) * 3
    return cum


n = int(input())
result = viralAdvertising(n)
print(result)
