def arrayManipulation(n, queries):
    arr = [0] * n
    for query in queries:
        a = query[0]
        b = query[1]
        k = query[2]
        try:
            arr[a - 1] += k
            arr[b] -= k
        except Exception:
            pass
    maxx, summ = 0, 0
    for i in range(n):
        summ += arr[i]
        maxx = max(maxx, summ)
    print(maxx)

nm = input().split()
n = int(nm[0])
m = int(nm[1])
queries = []
for _ in range(m):
    queries.append(list(map(int, input().rstrip().split())))
arrayManipulation(n, queries)
