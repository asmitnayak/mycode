from itertools import combinations


def acmTeam(topic):
    comb = list(combinations(topic, 2))
    team = []
    # print(comb)
    for i in comb:
        p1 = i[0]
        p2 = i[1]
        team.append(bin(p1 | p2).count('1'))
    print(max(team))
    print(team.count(max(team)))


nm = input().split()
n = int(nm[0])
m = int(nm[1])
topic = []
for _ in range(n):
    topic_item = int(input(), 2)
    topic.append(topic_item)
result = acmTeam(topic)
