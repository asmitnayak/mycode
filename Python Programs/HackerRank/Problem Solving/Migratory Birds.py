def migratoryBirds(ar):
    occur = {}
    for i in ar:
        if occur.__contains__(i):
            occur[i] += 1
        else:
            occur[i] = 1

    key_max = max(occur.keys(), key=(lambda k: occur[k]))
    return key_max


ar_count = int(input())
ar = list(map(int, input().rstrip().split()))
result = migratoryBirds(ar)

print(result)
