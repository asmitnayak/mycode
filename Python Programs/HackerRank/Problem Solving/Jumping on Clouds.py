def jumpingOnClouds(c):
    i = 0
    cnt = 0
    while i < len(c) - 1:
        if i + 2 < len(c) and c[i + 2] == 0:
            i += 2
        else:
            i += 1
        cnt += 1
    print(cnt)


n = int(input())
c = list(map(int, input().rstrip().split()))
result = jumpingOnClouds(c)
