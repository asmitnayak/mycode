def pageCount(n, p):
    p1 = p // 2
    p2 = p1 * 2
    return min(p // 2, (n - p2) // 2)


n = int(input())
p = int(input())
result = pageCount(n, p)
print(result)
