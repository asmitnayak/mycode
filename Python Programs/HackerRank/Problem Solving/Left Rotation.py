nd = input().split()
n = int(nd[0])
d = int(nd[1])
a = list(map(int, input().rstrip().split()))
b = a.copy()
for i in range(n):
    pos = (i - d) % n
    b[pos] = a[i]

print(b)
