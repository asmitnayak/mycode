def staircase(n):
    for i in range(n - 1, -1, -1):
        # Print Spaces
        print(" " * i, end="")
        # Print #
        print("#" * (n - i))

n = int(input())
staircase(n)
