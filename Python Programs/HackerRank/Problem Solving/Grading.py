def gradingStudents(grades):
    newGrd = []
    for i in range(len(grades)):
        if grades[i] < 38:
            newGrd.append(grades[i])
        elif ((grades[i] // 5) + 1) * 5 - grades[i] < 3:
            newGrd.append(((grades[i] // 5) + 1) * 5)
        else:
            newGrd.append(grades[i])
    return newGrd


n = int(input())

grades = []

for _ in range(n):
    grades_item = int(input())
    grades.append(grades_item)

result = gradingStudents(grades)

print(result, sep="\n")
