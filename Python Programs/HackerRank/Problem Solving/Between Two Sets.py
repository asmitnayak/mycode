from math import gcd


def getTotalX(a, b):
    lcm = a[0]
    for i in a[1:]:
        lcm = int(lcm * i / gcd(lcm, i))
    mulA = int(lcm)
    if min(b) % mulA != 0:
        return 0
    lst = []
    itr = int(min(b) / mulA)
    for i in range(1, itr + 1):
        k = 0
        for j in range(len(b)):
            if b[j] % (mulA * i) == 0:
                k += 1
        if k == len(b):
            lst.append(mulA * i)
    return len(lst)


nm = input().split()

n = int(nm[0])

m = int(nm[1])

a = list(map(int, input().rstrip().split()))

b = list(map(int, input().rstrip().split()))

total = getTotalX(a, b)
print(total)
