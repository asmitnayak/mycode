import itertools


def nonDivisibleSubset(k, s):
    subset = {}
    for i in s:
        rem = i % k
        if subset.keys().__contains__(rem):
            subset[rem] += 1
        else:
            subset[rem] = 1
    maxx = 0
    print(subset)
    for i in range(1, (k + 1) // 2):
        if i == 0:
            continue
        maxx += max(subset[i], subset[k - i])
    print(maxx)


nk = input().split()
n = int(nk[0])
k = int(nk[1])
S = list(map(int, input().rstrip().split()))
result = nonDivisibleSubset(k, S)
