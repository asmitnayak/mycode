def taumBday(b, w, bc, wc, z):
    b_cost, w_cost = bc, wc
    if bc > wc + z:
        b_cost = wc + z
        w_cost = wc
    elif wc > bc + z:
        b_cost = bc
        w_cost = bc + z
    print(b * b_cost + w * w_cost)


t = int(input())
for t_itr in range(t):
    bw = input().split()
    b = int(bw[0])
    w = int(bw[1])
    bcWcz = input().split()
    bc = int(bcWcz[0])
    wc = int(bcWcz[1])
    z = int(bcWcz[2])
    taumBday(b, w, bc, wc, z)
