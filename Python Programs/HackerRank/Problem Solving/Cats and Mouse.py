def catAndMouse(a, b, m):
    distA = abs(a - m)
    distB = abs(b - m)
    if distA > distB:
        return "Cat B"
    elif distA < distB:
        return "Cat A"
    else:
        return "Mouse C"


q = int(input())
for q_itr in range(q):
    xyz = input().split()
    x = int(xyz[0])
    y = int(xyz[1])
    z = int(xyz[2])

    result = catAndMouse(x, y, z)
    print(result)
