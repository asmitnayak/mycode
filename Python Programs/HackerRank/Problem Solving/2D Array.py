arr = []

for _ in range(6):
    arr.append(list(map(int, input().rstrip().split())))

row = len(arr)
col = len(arr[0])

hourglass = []

for i in range(1, row - 1):
    for j in range(1, col - 1):
        center = arr[i][j]
        top_three = arr[i - 1][j - 1:j + 2]
        bottom_three = arr[i + 1][j - 1:j + 2]
        summ = sum(top_three) + sum(bottom_three) + center
        hourglass.append(summ)

print(max(hourglass))
