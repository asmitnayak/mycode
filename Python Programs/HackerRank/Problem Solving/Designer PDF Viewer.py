def designerPdfViewer(h, word):
    maxi = h[0]
    for i in word:
        maxi = max(h[ord(i) - 97], maxi)
    print(maxi * len(word))


h = list(map(int, input().rstrip().split()))
word = input()
result = designerPdfViewer(h, word)
