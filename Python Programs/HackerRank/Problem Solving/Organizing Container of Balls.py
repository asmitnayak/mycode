#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the organizingContainers function below.


def organizingContainers(container):
    n = len(container)
    balls = [0] * n
    for i in container:
        for j in range(n):
            balls[j] += i[j]
    print(balls)

if __name__ == '__main__':

    q = int(input())

    for q_itr in range(q):
        n = int(input())

        container = []

        for _ in range(n):
            container.append(list(map(int, input().rstrip().split())))

        result = organizingContainers(container)
