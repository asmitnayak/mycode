def move(r_q, c_q, r_shift, c_shift):
    global obstacles
    global n
    block = 0
    row, col = r_q + r_shift, c_q + c_shift
    while 1 <= row <= n and 1 <= col <= n:
        try:
            tf = obstacles[(row, col)]
            break
        except KeyError:
            block += 1
            row += r_shift
            col += c_shift
    return block


def queensAttack(n, k, r_q, c_q):
    path = 0
    path += move(r_q, c_q, 1, 0)  # Vertical Up
    path += move(r_q, c_q, -1, 0)  # Vertical Down
    path += move(r_q, c_q, 0, 1)  # Horizontal Right
    path += move(r_q, c_q, 0, -1)  # Horizontal Left
    path += move(r_q, c_q, 1, 1)  # Right-Upper Diagonal
    path += move(r_q, c_q, -1, 1)  # Right-Lower Diagonal
    path += move(r_q, c_q, 1, -1)  # Left-Upper Diagonal
    path += move(r_q, c_q, -1, -1)  # Left-Lower Diagonal
    print(path)


nk = input().split()
n = int(nk[0])
k = int(nk[1])
r_qC_q = input().split()
r_q = int(r_qC_q[0])
c_q = int(r_qC_q[1])
obstacles = {}
for _ in range(k):
    obstacles[tuple(map(int, input().rstrip().split()))] = False
result = queensAttack(n, k, r_q, c_q)
