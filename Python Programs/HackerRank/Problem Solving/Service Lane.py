def serviceLane(width, cases):
    for i in cases:
        print(min(width[i[0]:i[1] + 1]))


nt = input().split()
n = int(nt[0])
t = int(nt[1])
width = list(map(int, input().rstrip().split()))
cases = []
for _ in range(t):
    cases.append(list(map(int, input().rstrip().split())))
result = serviceLane(width, cases)
