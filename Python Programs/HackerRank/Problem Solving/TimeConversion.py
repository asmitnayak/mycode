def timeConversion(s):
    hh = int(s[:s.index(":")])
    mm = s[s.index(":") + 1:s.rindex(":")]
    ss = s[s.rindex(":") + 1:s.rindex(":") + 3]
    if s.__contains__("PM"):
        Mhh = (hh + 12) if hh < 12 else hh
        if Mhh > 24:
            Mhh -= 24
        H = str(Mhh) if Mhh > 9 else "0" + str(Mhh)
        time = H + ":" + mm + ":" + ss
        return time
    else:
        Mhh = 0 if hh == 12 else hh
        H = str(Mhh) if Mhh > 9 else "0" + str(Mhh)
        time = H + ":" + mm + ":" + ss
        return time

print(timeConversion(input()))
