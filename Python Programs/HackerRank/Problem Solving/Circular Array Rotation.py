def circularArrayRotation(a, k, queries):
    newArr = a.copy()
    pos = k
    for i in range(len(a)):
        if i + pos < len(a):
            newArr[i + pos] = a[i]
        else:
            newArr[(i + pos) % len(a)] = a[i]
    for i in queries:
        print(newArr[i])


nkq = input().split()

n = int(nkq[0])

k = int(nkq[1])

q = int(nkq[2])

a = list(map(int, input().rstrip().split()))

queries = []

for _ in range(q):
    queries_item = int(input())
    queries.append(queries_item)

result = circularArrayRotation(a, k, queries)
