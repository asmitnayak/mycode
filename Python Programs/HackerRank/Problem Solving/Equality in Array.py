def equalizeArray(arr):
    maxx = 0
    for i in list(set(arr)):
        m = arr.count(i)
        if m > maxx:
            maxx = m
    print(len(arr) - maxx)

n = int(input())
arr = list(map(int, input().rstrip().split()))
result = equalizeArray(arr)
