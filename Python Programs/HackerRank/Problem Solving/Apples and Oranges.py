def countApplesAndOranges(s, t, a, b, apples, oranges):
    ap, org = 0, 0
    for i in range(len(apples)):
        if s <= apples[i] + a <= t:
            ap += 1
    for i in range(len(oranges)):
        if s <= oranges[i] + b <= t:
            org += 1
    print(ap, org, sep="\n")


st = input().split()
s = int(st[0])
t = int(st[1])
ab = input().split()
a = int(ab[0])
b = int(ab[1])
mn = input().split()
m = int(mn[0])
n = int(mn[1])

apples = list(map(int, input().rstrip().split()))
oranges = list(map(int, input().rstrip().split()))

countApplesAndOranges(s, t, a, b, apples, oranges)
