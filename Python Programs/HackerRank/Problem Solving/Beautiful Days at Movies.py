def beautifulDays(i, j, k):
    c = 0
    for itr in range(i, j + 1):
        curr = str(itr)
        rev = curr[::-1]
        print(curr, rev, "\n")
        diff = abs(int(curr) - int(rev))
        if diff % k == 0:
            c += 1
    print(c)


ijk = input().split()

i = int(ijk[0])

j = int(ijk[1])

k = int(ijk[2])

result = beautifulDays(i, j, k)
