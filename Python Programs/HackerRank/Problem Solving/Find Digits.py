def findDigits(n):
    num = int(n)
    lst = list(map(int, list(n)))
    dividor = 0
    for d in lst:
        if d != 0 and num % d == 0:
            dividor += 1
    print(dividor)


t = int(input())

for t_itr in range(t):
    n = input()
    result = findDigits(n)
