t = int(input())

for t_itr in range(t):
    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    inp = input().rstrip().split()

    time = {"On Time": 0, "Late": 0}

    for i in inp:
        if int(i) <= 0:
            time["On Time"] += 1
        else:
            time["Late"] += 1

    if time["On Time"] >= k:
        print("NO")
    else:
        print("YES")
