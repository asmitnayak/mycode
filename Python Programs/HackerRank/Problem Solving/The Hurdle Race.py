def hurdleRace(k, height):
    mht = max(height)
    if mht - k > 0:
        print(mht - k)
    else:
        print(0)

nk = input().split()
n = int(nk[0])
k = int(nk[1])
height = list(map(int, input().rstrip().split()))
result = hurdleRace(k, height)
