def utopianTree(n):
    H = 1
    for i in range(n):
        if i % 2 == 0:
            H = H * 2
        else:
            H += 1
    return H

t = int(input())

for t_itr in range(t):
    n = int(input())

    result = utopianTree(n)
    print(result)
