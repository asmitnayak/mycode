def kangaroo(x1, v1, x2, v2):
    num = x1 - x2
    den = v2 - v1
    if den == 0:
        return "NO"
    n = num / den
    if n > 0 and den != 0:
        if num % den == 0:
            print("YES")
        else:
            print("NO")
    else:
        print("NO")


x1V1X2V2 = input().split()

x1 = int(x1V1X2V2[0])

v1 = int(x1V1X2V2[1])

x2 = int(x1V1X2V2[2])

v2 = int(x1V1X2V2[3])

result = kangaroo(x1, v1, x2, v2)
