def repeatedString(s, n):
    occur = s.count('a')
    if occur == 0:
        return 0
    size = len(s)
    mul = n // size
    cnt = mul * occur
    cnt += s[:n % size].count('a')
    return cnt


s = list(input())
n = int(input())
result = repeatedString(s, n)
print(result)
