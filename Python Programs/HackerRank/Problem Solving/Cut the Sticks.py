def removeZeros(arr):
    while 0 in arr:
        arr.remove(0)
    return arr


def cutTheSticks(arr):
    while len(arr) > 0:
        mini = min(arr)
        l = len(arr)
        print(l)
        for j in range(len(arr)):
            arr[j] -= mini
        arr = removeZeros(arr)


n = int(input())
arr = list(map(int, input().rstrip().split()))
result = cutTheSticks(arr)
