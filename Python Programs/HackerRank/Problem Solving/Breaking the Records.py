def breakingRecords(scores):
    mini = maxi = scores[0]
    maxFlag = minFlag = 0
    for i in scores[1:]:
        if i > maxi:
            maxi = i
            maxFlag += 1
        if i < mini:
            mini = i
            minFlag += 1
    return (maxFlag, minFlag)


n = int(input())
scores = list(map(int, input().rstrip().split()))
result = breakingRecords(scores)

print(result)
