def permutationEquation(p):
    pos = []
    for i in range(1, len(p) + 1):
        x = p.index(i) + 1
        px = p.index(x) + 1
        pos.append(px)
    print('\n'.join(map(str, pos)))


n = int(input())

p = list(map(int, input().rstrip().split()))

result = permutationEquation(p)
