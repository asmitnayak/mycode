#!/bin/python3

import math
import os
import random
import re
import sys


# Complete the matrixRotation function below.
def matrixRotation(matrix, rot):
    atop = 0
    abottom = len(matrix) - 1
    aleft = 0
    aright = len(matrix[0]) - 1
    while atop < abottom and aleft < aright:

        perimeter = 2 * (aright - aleft + 1) + 2 * (abottom - atop + 1 - 2)
        perimeter = rot % (perimeter)
        top = atop
        bottom = abottom
        left = aleft
        right = aright
        for _ in range(perimeter):
            prev = matrix[bottom - 1][left]
            for i in range(left, right + 1):
                curr = matrix[bottom][i]
                matrix[bottom][i] = prev
                prev = curr
            bottom -= 1
            for i in range(bottom, top - 1, -1):
                curr = matrix[i][right]
                matrix[i][right] = prev
                prev = curr
            right -= 1
            for i in range(right, left - 1, -1):
                curr = matrix[top][i]
                matrix[top][i] = prev
                prev = curr
            top += 1
            for i in range(top, bottom + 1):
                curr = matrix[i][left]
                matrix[i][left] = prev
                prev = curr
            left += 1
            top = atop
            bottom = abottom
            left = aleft
            right = aright
        atop += 1
        abottom -= 1
        aleft += 1
        aright -= 1
    return matrix


if __name__ == '__main__':
    mnr = input().split()

    m = int(mnr[0])

    n = int(mnr[1])

    r = int(mnr[2])

    matrix = []
    for _ in range(m):
        matrix.append([int(x) for x in input().strip().split(" ")])

    matrix = matrixRotation(matrix, r)
    for row in matrix:
        prn = ' '.join(map(str, row))
        print(prn)
