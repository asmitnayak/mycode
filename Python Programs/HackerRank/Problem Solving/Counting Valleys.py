def countingValleys(n, s):
    valley = 0
    curr = 0
    for i in s:
        if i == 'U':
            curr += 1
        else:
            curr -= 1
        if curr == -1 and i == 'D':
            valley += 1
    return valley


n = int(input())
s = input()
result = countingValleys(n, s)
print(result)
