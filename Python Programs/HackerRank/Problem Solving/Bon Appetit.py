def bon_appetit(k, bill, b):
    cost = sum(bill) - bill[k]
    cost /= 2
    if cost == b:
        print("Bon Appetit")
    else:
        print(int(b - cost))


n, k = map(int, input().split())
bill = list(map(int, input().split()))
b = int(input())

bon_appetit(k, bill, b)
