def sockMerchant(n, ar):
    cols = {}
    pairs = 0
    for i in ar:
        if cols.__contains__(i):
            cols[i] += 1
        else:
            cols[i] = 1
    for i in list(cols.keys()):
        pairs = pairs + (cols[i] // 2)
    return pairs


n = int(input())
ar = list(map(int, input().rstrip().split()))
result = sockMerchant(n, ar)
print(result)
