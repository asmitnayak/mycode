def pickingNumbers(a):
    lst = []
    a.sort()
    for i in range(len(a)):
        l = 0    # dont change 1 and change one mini so the range is valid
        mini = mini2 = a[i]
        for j in range(len(a)):
            if mini - 1 <= a[j] <= mini + 1 and mini2 - 1 <= a[j] <= mini2 + 1:
                l += 1
                mini = min(a[j], mini)
        lst.append(l)
    return max(lst)


n = int(input())
a = list(map(int, input().rstrip().split()))
result = pickingNumbers(a)
print(result)
