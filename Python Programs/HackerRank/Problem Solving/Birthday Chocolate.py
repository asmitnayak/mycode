def solve(s, d, m):
    flag = 0
    pos = 0
    for i in range(0, len(s) - m + 1):
        add = sum(s[pos:i + m])
        pos += 1
        if add == d:
            flag += 1
    print(flag)


n = int(input())

s = list(map(int, input().rstrip().split()))

dm = input().split()

d = int(dm[0])

m = int(dm[1])

result = solve(s, d, m)
