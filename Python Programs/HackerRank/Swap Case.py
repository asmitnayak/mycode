def swap_case(s):
    newStr = ""
    for i in range(len(s)):
        newStr = newStr + (s[i].lower() if s[i].isupper() else s[i].upper())
    return newStr


s = input()
result = swap_case(s)
print(result)
