def print_rangoli(n):
    ch = chr(ord('a') + n - 1)
    copy = n - 1
    for i in range(n):
        for j in range(copy * 2):
            print("-", sep="", end="")
        for k in range(ord(ch), ord(ch) - i, -1):
            print(chr(k), end="")
            if k > ord(ch) - i:
                print("-", end="")
        for o in range(ord(ch) - i, ord(ch) + 1):
            print(chr(o), end="")
            if o < ord(ch):
                print("-", end="")
        for j in range(copy * 2):
            print("-", sep="", end="")
        copy -= 1
        print("")
    copy = 1
    for i in range(n - 2, -1, -1):
        for j in range(copy * 2):
            print("-", sep="", end="")
        for k in range(ord(ch), ord(ch) - i - 1, -1):
            print(chr(k), end="")
            if k > ord(ch) - i - 1 and i != 0:
                print("-", end="")
        for o in range(ord(ch) - i + 1, ord(ch) + 1):
            print(chr(o), end="")
            if o < ord(ch):
                print("-", end="")
        for j in range(copy * 2):
            print("-", sep="", end="")
        copy += 1
        print("")

n = int(input())
print_rangoli(n)
