def findCommon(s, t):
    c = 0
    for i in range(min(len(s), len(t))):
        if s[i] == t[i]:
            c += 1
        else:
            break
    return c


def appendAndDelete(s, t, k):
    com = findCommon(s, t)
    delete = len(s) - com
    add = len(t) - com
    if add + delete != k:
        if delete == 0:
            if add == 0:
                if 2 * len(s) <= k:
                    print("Yes")
                else:
                    print("No")
            elif add > k:
                print("No")
            elif (k - add) % 2 == 0:
                print("Yes")
            else:
                print("No")
        elif add == 0:
            if delete > k:
                print("No")
            elif (k - delete - 1) % 2 == 0:
                print("Yes")
            else:
                print("No")
        else:
            op = k - delete
            if add > op:
                print("No")
            else:
                print("Yes")
    elif add + delete == k:
        print("Yes")


s = input()

t = input()

k = int(input())

result = appendAndDelete(s, t, k)
