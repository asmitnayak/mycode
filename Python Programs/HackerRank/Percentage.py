from decimal import Decimal

n = int(input())
student_marks = {}
for _ in range(n):
    name, *line = input().split()
    scores = list(map(float, line))
    student_marks[name] = scores
query_name = input()
avg = 0.0
for i in range(0, 3):
    avg += student_marks[query_name][i]

avg = Decimal(avg) / Decimal(3.0)
print(round(avg, 2))
