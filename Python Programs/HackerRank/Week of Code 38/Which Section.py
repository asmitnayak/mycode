def whichSection(n, k, a):
    total = 0
    for i in range(len(a)):
        total += a[i]
        if k <= total:
            return i + 1


t = int(input())

for t_itr in range(t):
    nkm = input().split()

    n = int(nkm[0])

    k = int(nkm[1])

    m = int(nkm[2])

    a = list(map(int, input().rstrip().split()))

    result = whichSection(n, k, a)

    print(result)
