inp = input("").split()
m, n = int(inp[0]), int(inp[1])
wel = "WELCOME"
design = ".|."
# Top Half
for i in range(m // 2):
    # Right half of "-"
    for j in range(int((n - 3) / 2) - 3 * i):
        print(end="-")
    # Printing design
    for k in range(i * 2 + 1):
        print(end=design)
    # Left half of "-"
    for l in range(int((n - 3) / 2) - 3 * i):
        print(end="-")
    print()

print(wel.center(n, '-'))

# Bottom Half
for i in range(m // 2 - 1, -1, -1):
    # Right half of "-"
    for j in range(int((n - 3) / 2) - 3 * i):
        print(end="-")
    # Printing design
    for k in range(i * 2 + 1):
        print(end=design)
    # Left half of "-"
    for l in range(int((n - 3) / 2) - 3 * i):
        print(end="-")
    print()
