def minimumBribes(q):
    swaps = 0
    flag = False
    for i in range(len(q)):
        ind = 0
        for j in range(len(q) - 1 - i):
            if q[j + 1] < q[j]:
                if ind >= 2:
                    flag = True
                    break
                ind += 1
                swaps += 1
                t = q[j + 1]
                q[j + 1] = q[j]
                q[j] = t
            else:
                ind = 0
        if flag:
            break
    if not flag:
        print(swaps)
    else:
        print("Too chaotic")


t = int(input())

for t_itr in range(t):
    n = int(input())

    q = list(map(int, input().rstrip().split()))

    minimumBribes(q)
