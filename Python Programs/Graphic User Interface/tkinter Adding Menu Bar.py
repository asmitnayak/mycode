from tkinter import *


class Window(Frame):
    def __init__(self, master=None):
        self.master = master
        Frame.__init__(self, master)
        self.init_window()

    def init_window(self):
        self.master.title("GUI")
        self.pack(fill=BOTH, expand=1)

        menu = Menu(self.master)
        self.master.config(menu=menu)

        file = Menu(menu, tearoff=False)
        file.add_command(label='Exit', command=self.client_exit)
        file.add_command(label='Save')
        menu.add_cascade(label='File', menu=file)

        edit = Menu(menu)
        edit.add_command(label='Undo')
        menu.add_cascade(label='Edit', menu=edit)

    def client_exit(self):
        self.master.destroy()

root = Tk()
root.geometry("400x300")
app = Window(root)
root.mainloop()
