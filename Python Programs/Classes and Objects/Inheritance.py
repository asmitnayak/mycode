class Employee:
    num_of_emp = 0
    raise_amount = 1.04

    def __init__(self, name, last, pay):
        self.name = name           # these r instance variables
        self.last = last
        self.pay = pay
        self.email = name + '.' + last + '@xyz.com'

        Employee.num_of_emp += 1

    def fullname(self):
        return '{} {}'.format(self.name, self.last)

    def apply_raise(self):
        self.pay = int(self.pay * Employee.raise_amount)


class Developer(Employee):
    pass


dev_1 = Developer('Hero', 'Zero', 50000)
dev_2 = Developer('Test', 'User', 60000)

print(dev_1.email)
print(dev_2.email)
