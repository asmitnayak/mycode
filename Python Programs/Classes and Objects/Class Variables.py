class Employee:
    num_of_emp = 0
    raise_amount = 1.04

    def __init__(self, name, last, pay):
        self.name = name           # these r instance variables
        self.last = last
        self.pay = pay
        self.email = name + '.' + last + '@xyz.com'

        Employee.num_of_emp += 1    # use of self.num_of_emp is useless as we want the number
        # of employees which will be denoted by the Employee.num_of_emp.

        """
        <class_name>.<variable_name> corresponds to static variables in other languages and is
        accesible to all class objects. <self>.<variable_name> corresponds to non-static variable
        in other languages and is distinct for each object
        Secondly changes to <class_name>.<variable_name> is reflected across all class objects.
        However, changes to <self>.<variable_name> is only for that object
        """

    def fullname(self):
        return '{} {}'.format(self.name, self.last)

    def apply_raise(self):
        self.pay = int(self.pay * Employee.raise_amount)    # can also be self.raise_amount if we wish
        # to calculate using the changed value for every single individual object


print(Employee.num_of_emp)

emp_1 = Employee('Hero', 'Zero', 50000)
emp_2 = Employee('Test', 'User', 60000)

print("Employee.num_of_emp =", Employee.num_of_emp)
print("Employee.raise_amount =", Employee.raise_amount)
print("emp_1.raise_amount =", emp_1.raise_amount)
print("emp_2.raise_amount =", emp_2.raise_amount)
# changing emp_1.raise_amount
emp_1.raise_amount = 1.05
print("emp_1.raise_amount =", emp_1.raise_amount)
print("Employee.raise_amount =", Employee.raise_amount, "NO CHANGE\n")
Employee.raise_amount = 1.09
print("emp_1.raise_amount =", emp_1.raise_amount, "NO CHANGE has become stagnant, ie, local and no longer references the static variable")
print("emp_2.raise_amount =", emp_2.raise_amount, "CHANGED as it still references the static variable")
print("Employee.raise_amount =", Employee.raise_amount, "NO CHANGE\n")
# changing
emp_2.raise_amount = 1.02
print("emp_1.raise_amount =", emp_1.raise_amount, "NO CHANGE has become stagnant, ie, local and no longer references the static variable")
print("emp_2.raise_amount =", emp_2.raise_amount, "CHANGED as it no longer references the static variable")
print("Employee.raise_amount =", Employee.raise_amount, "NO CHANGE\n")
