class Employee:
    def __init__(self, name, last, pay):    # constructor
        self.name = name
        self.last = last
        self.pay = pay
        self.email = name + '.' + last + '@xyz.com'

    def fullname(self):
        return '{} {}'.format(self.name, self.last)


emp_1 = Employee('Hero', 'Zero', 50000)
emp_2 = Employee('Test', 'User', 60000)

print(emp_1)
print(emp_2)


print(emp_1.email)
print(emp_2.email)

print(emp_1.fullname())     # Background: Employee.fullname(emp_1)

'''
If in def fullname() no self is given then it gives error saying that one positional argument was given.
this is the object itself. In reality the background working is: Employee.fullname(emp_1)
'''


'''
# tedious

emp_1.name = 'Hero'
emp_1.last = 'Zero'
emp_1.email = 'hero.zero@xyz.com'
emp_1.pay = 50000

emp_2.name = 'Tese'
emp_2.last = 'User'
emp_2.email = 'test.user@xyz.com'
emp_2.pay = 60000
'''
