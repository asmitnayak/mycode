import datetime


class Employee:
    num_of_emp = 0
    raise_amount = 1.04

    def __init__(self, name, last, pay):
        self.name = name
        self.last = last
        self.pay = pay
        self.email = name + '.' + last + '@xyz.com'

        Employee.num_of_emp += 1

    def fullname(self):     # regular methods -> takes the instance as the first argument
        return '{} {}'.format(self.name, self.last)

    def apply_raise(self):
        self.pay = int(self.pay * Employee.raise_amount)

    @classmethod
    def set_raise_amount(cls, amount):  # class method denoted by a decorator. takes the class as the
        # first input
        cls.raise_amount = amount

    @classmethod
    def from_string(cls, emp_str):
        first, last, pay = emp_str.split('-')
        return cls(first, last, pay)  # cls(first, last, pay) creates a new object and returns it

    @staticmethod
    def is_workday(day):    # Static method and unlike classmethods or regular methods doesnot pass
        # either a class or the instance automatically
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True


emp_1 = Employee('Hero', 'Zero', 50000)
emp_2 = Employee('Test', 'User', 60000)

Employee.set_raise_amount(1.05)  # updates the value of class variable raise_amount could also be called
# using <instance_name>.set_raise_amount(x) it would still update the class variable and not the
# instance variable
"""
If an instance has already changed the variable then no change is reflected when the class variable is
changed
"""
print(Employee.raise_amount)
print(emp_1.raise_amount)
print(emp_2.raise_amount)

new_emp = Employee.from_string('John-Doe-70000')
print(new_emp.email)
print(new_emp.pay)


my_date = datetime.date(2016, 7, 10)    # yyyy, mm, dd
print(Employee.is_workday(my_date))
my_date = datetime.date(2016, 7, 11)
print(Employee.is_workday(my_date))
