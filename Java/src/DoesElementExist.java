import java.util.ArrayList;
import java.util.Scanner;

public class DoesElementExist {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        ArrayList<Integer> a[] = new ArrayList[n];

        for(int i = 0; i < n; i++){
            int len = sc.nextInt();
            a[i] = new ArrayList<Integer>();
            for(int j = 0; j < len; j++) {
                a[i].add(sc.nextInt());
            }
        }

        int q = sc.nextInt();

        int query[][] = new int[q][2];
        for(int i = 0; i < q; i++){
            query[i][0] = sc.nextInt();
            query[i][1] = sc.nextInt();
        }

        for(int i = 0; i < q; i++){
            try {
                int ele = a[query[i][0]-1].get(query[i][1]-1);
                System.out.println(ele);
            }catch(IndexOutOfBoundsException e){
                System.out.println("ERROR!");
            }
        }

    }
}
