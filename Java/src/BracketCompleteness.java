import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class BracketCompleteness {

    private static boolean checkParenthesis(String a, String b){
        if(a.equalsIgnoreCase("("))
            return b.equalsIgnoreCase(")");
        else if(a.equalsIgnoreCase("{"))
            return b.equalsIgnoreCase("}");
        else
            return b.equalsIgnoreCase("]");
    }

    public static void main(String []argh)
    {
        Scanner sc = new Scanner(System.in);

        ArrayList<String> start = new ArrayList<>();
        start.add("(");
        start.add("{");
        start.add("[");

        ArrayList<String> finish = new ArrayList<>();
        finish.add(")");
        finish.add("}");
        finish.add("]");

        while (sc.hasNext()) {
            boolean flag = true;
            String inputx = sc.next();
            Stack<String> brkt = new Stack<>();
            String arr[] = inputx.split("");
            for(int i = 0; i < arr.length; i++) {
                String input = arr[i];
                if (brkt.isEmpty() && finish.contains(input)) {
                    flag = false;
                    break;
                }
                if (finish.contains(input)) {
                    if (checkParenthesis(brkt.peek(), input))
                        brkt.pop();
                } else if (start.contains(input))
                    brkt.push(input);
                else {
                    flag = false;
                    break;
                }
            }
            if(!brkt.empty())
                flag = false;
                System.out.println(flag);
            }
        }
    }

