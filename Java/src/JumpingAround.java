import java.util.Scanner;

public class JumpingAround {
    private static int L;
    private static int[] G;
    public static boolean canWin(int leap, int[] game) {
        L = leap;
        G = game;
        return play(0);
    }

    private static int cnt = 1;

    private static boolean play(int pos){
        if(pos < 0)
            return false;
        if(G[pos] != 0)
            return false;
        G[pos] = 1;
        if(pos+1 >= G.length || pos+L >= G.length)
            return true;
        boolean flag1 = play(pos+1);
        boolean flag2 = false;
        if(L > 0)
            flag2 = play(pos+L);
        boolean flag3 = false;
        if(flag2 && pos+L-1 != pos)
            flag3 = play(pos+L-1);
        return flag1 || flag2 || flag3;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println(  ((canWin(leap, game)) ? "YES" : "NO" ));
            cnt++;
        }
        scan.close();
    }
}
