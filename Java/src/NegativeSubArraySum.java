import java.util.Scanner;

public class NegativeSubArraySum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int arr[] = new int[n];

        for(int i = 0; i < n; i++)
            arr[i] = sc.nextInt();

        int len = 0, neg = 0;
        while (len < n) {
            for (int i = 0; i < n - len; i++) {
                int sum = 0;
                for (int j = 0; j <= len; j++)
                    sum += arr[i + j];
                if (sum < 0)
                    neg++;
            }
            len++;
        }
        System.out.print(neg);
    }
}
